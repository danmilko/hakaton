﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkGroupApp
{
    public class Goal
    {
        private int id;
        public string name;
        public string description;
        public int status;
        public int Id
        {
            get
            {
                return id;
            }
        }
        public Goal(int _id, string _name, string _description, int _status)
        {
            id = _id;
            name = _name;
            description = _description;
            status = _status;
        }
        public Goal()
        {

        }
    }
}

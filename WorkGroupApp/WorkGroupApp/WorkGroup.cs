﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkGroupApp
{
    public class WorkGroup
    {
        private int id;
        public List<Worker> workers;
        public string name;
        public List<Goal> goals;
        public string comments;
        public string Name
        {
            get
            {
                return name;
            }
        }
        public int Id
        {
            get
            {
                return id;
            }
        }
        public WorkGroup(int _id, List<Worker> _workgroup, string _name, List<Goal> _goals, string _comments)
        {
            id = _id;
            workers = _workgroup;
            name = _name;
            goals = _goals;
            comments = _comments;
        }
        public WorkGroup()
        {
            workers = new List<Worker>();
            goals = new List<Goal>();
        }
        public string GetGoalNames()
        {
            string res = string.Empty;
            for (int i = 0; i < goals.Count; i++)
            {
                res += goals[i].name + ",";
            }
            if (res.Length > 0)
            {
                res = res.Remove(res.Length - 1);
            }
            return res;
        }
    }
}

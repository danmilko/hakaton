﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkGroupApp
{
    public class Sprav
    {
        public int id;
        public string name;
        public Sprav(int _id, string _name)
        {
            id = _id;
            name = _name;
        }
        public Sprav()
        {
        }
    }
}

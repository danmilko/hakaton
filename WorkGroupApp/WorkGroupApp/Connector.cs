﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data;
using System.Data;

namespace WorkGroupApp
{
    public class Connector
    {
        string connectionString = "server=127.0.0.1;user=root;database=WorkGroupApp;port=3306;password=;";
        string sqlQuery;
        MySqlConnection connection;
        MySqlDataAdapter adapter;
        MySqlCommand cmd;
        public DataTable GetDataTable(int index)
        {
            DataTable dt = new DataTable();
            connection = new MySqlConnection(connectionString);
            connection.Open();
            switch (index)
            {
                case 1:
                    sqlQuery = "SELECT * FROM Workers";
                    adapter = new MySqlDataAdapter(sqlQuery, connection);
                    adapter.Fill(dt);
                    break;
                case 2:
                    sqlQuery = "SELECT * FROM dolgnosti";
                    adapter = new MySqlDataAdapter(sqlQuery, connection);
                    adapter.Fill(dt);
                    break;
                case 3:
                    sqlQuery = "SELECT * FROM skills";
                    adapter = new MySqlDataAdapter(sqlQuery, connection);
                    adapter.Fill(dt);
                    break;
                case 4:
                    sqlQuery = "SELECT * FROM WorkGroups";
                    adapter = new MySqlDataAdapter(sqlQuery, connection);
                    adapter.Fill(dt);
                    break;
            }
            connection.Close();
            return dt;
        }
        public int Login(string email, string password)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT COUNT(*) FROM Workers WHERE email = \"" + email + "\" AND password = \"" + password + "\";";
            cmd = new MySqlCommand(sqlQuery, connection);
            int res = int.Parse(cmd.ExecuteScalar().ToString());
            if (res > 0) 
            {
                sqlQuery = "SELECT id FROM Workers WHERE email = \"" + email + "\" AND password = \"" + password + "\";";
                cmd = new MySqlCommand(sqlQuery, connection);
                res = int.Parse(cmd.ExecuteScalar().ToString());
                connection.Close();
                return res;
            }
            else 
            {
                connection.Close();
                return 0;
            }
        }
        public List<Worker> GetWorkers()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT * FROM Workers";
            adapter = new MySqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            List<Worker> res = new List<Worker>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Worker worker = new Worker(int.Parse(dt.Rows[i].ItemArray[0].ToString()), 
                    int.Parse(dt.Rows[i].ItemArray[1].ToString()), 
                    dt.Rows[i].ItemArray[2].ToString(), 
                    int.Parse(dt.Rows[i].ItemArray[3].ToString()), 
                    dt.Rows[i].ItemArray[4].ToString(), 
                    dt.Rows[i].ItemArray[5].ToString(), 
                    dt.Rows[i].ItemArray[6].ToString(), 
                    dt.Rows[i].ItemArray[7].ToString(),
                    int.Parse(dt.Rows[i].ItemArray[8].ToString()),
                    int.Parse(dt.Rows[i].ItemArray[9].ToString()),
                    dt.Rows[i].ItemArray[10].ToString(),
                    dt.Rows[i].ItemArray[11].ToString(),
                    int.Parse(dt.Rows[i].ItemArray[12].ToString()),
                    int.Parse(dt.Rows[i].ItemArray[13].ToString()));
                res.Add(worker);
            }
            connection.Close();
            return res;
        }
        public List<WorkGroup> GetWorkGroups()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT * FROM WorkGroups";
            adapter = new MySqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            List<WorkGroup> res = new List<WorkGroup>();
            List<Worker> workers = new List<Worker>();
            List<Goal> goals = new List<Goal>();
            workers = GetWorkers();
            goals = GetGoals();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string workerslist = dt.Rows[i].ItemArray[4].ToString();
                List<Worker> workgroup = new List<Worker>();
                for (int j = 0; j < workerslist.Length; j++)
                {
                    for (int l = 0; l < workers.Count; l++)
                    {
                        if (int.Parse(workerslist[j].ToString()) == workers[l].Id)
                        {
                            workgroup.Add(workers[l]);
                        }
                    }
                }
                string goalslist = dt.Rows[i].ItemArray[2].ToString();
                List<Goal> goalgroup = new List<Goal>();
                for (int j = 0; j < goalslist.Length; j++)
                {
                    for (int l = 0; l < goals.Count; l++)
                    {
                        if (int.Parse(goalslist[j].ToString()) == goals[l].Id)
                        {
                            goalgroup.Add(goals[l]);
                        }
                    }
                }
                WorkGroup wg = new WorkGroup(int.Parse(dt.Rows[i].ItemArray[0].ToString()), workgroup,
                    dt.Rows[i].ItemArray[1].ToString(),
                    goalgroup,
                    dt.Rows[i].ItemArray[3].ToString());
                res.Add(wg);
            }
            connection.Close();
            return res;
        }
        public List<Sprav> GetAllSkills()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT * FROM skills";
            adapter = new MySqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            List<Sprav> res = new List<Sprav>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                res.Add(new Sprav(int.Parse(dt.Rows[i].ItemArray[0].ToString()),
                    dt.Rows[i].ItemArray[1].ToString()));
            }
            connection.Close();
            return res;
        }
        public List<Sprav> GetAllDolgnost()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT * FROM dolgnosti";
            adapter = new MySqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            List<Sprav> res = new List<Sprav>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                res.Add(new Sprav(int.Parse(dt.Rows[i].ItemArray[0].ToString()),
                    dt.Rows[i].ItemArray[1].ToString()));
            }
            connection.Close();
            return res;
        }
        public void SetOnline(Worker worker, int online)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "UPDATE Workers SET online = \"" + online + "\" WHERE id = " + worker.Id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
        }
        public string ConvertToDate(int time)
        {
            string res = new DateTime(1970, 1, 1, 0, 0, 0).AddDays(time).ToShortDateString().ToString();
            return res;
        }
        public int ConvertToUnixTime(DateTime time)
        {
            int unixTime = Convert.ToInt32((time - new System.DateTime(1970, 1, 1, 3, 0, 0, 0)).TotalDays);
            return unixTime;
        }
        public string GetSkill(int index)
        {
            List<Sprav> skills = GetAllSkills();
            for (int i = 0; i < skills.Count; i++)
            {
                if (skills[i].id == index)
                {
                    return skills[i].name;
                }
            }
            return String.Empty;
        }
        public string GetDolgnost(int index)
        {
            List<Sprav> dolgnosti = GetAllDolgnost();
            for (int i = 0; i < dolgnosti.Count; i++)
            {
                if (dolgnosti[i].id == index)
                {
                    return dolgnosti[i].name;
                }
            }
            return String.Empty;
        }
        public string ConvertToWorkGroupsNames(string workgroups)
        {
            string res = String.Empty;
            List<WorkGroup> workgroupslist = GetWorkGroups();
            for (int i = 0; i < workgroups.Length; i++)
            {
                for (int j = 0; j < workgroupslist.Count; j++)
                {
                    if (int.Parse(workgroups[i].ToString()) == workgroupslist[i].Id)
                    {
                        res += workgroupslist[i].name + ",";
                    }
                }
            }
            if (res.Length > 0)
            {
                res = res.Remove(res.Length - 1);
            }
            return res;
        }
        public string ConvertToWorkersNames(string workers)
        {
            string res = String.Empty;
            List<Worker> workerslist = GetWorkers();
            for (int i = 0; i < workers.Length; i++)
            {
                for (int j = 0; j < workerslist.Count; j++)
                {
                    if (int.Parse(workers[i].ToString()) == workerslist[i].Id)
                    {
                        res += workerslist[i].name + ",";
                    }
                }
            }
            res = res.Remove(res.Length - 1);
            return res;
        }
        public string ConvertToWorkersIds(List<Worker> workers)
        {
            string res = string.Empty;
            for (int i = 0; i < workers.Count; i++)
            {
                res += workers[i].Id;
            }
            return res;
        }
        public string ConvertToGoalNames(string goals)
        {
            string res = string.Empty;
            List<Goal> goallist = GetGoals();
            for (int i = 0; i < goals.Length; i++)
            {
                for (int j = 0; j < goallist.Count; j++)
                {
                    if (int.Parse(goals[i].ToString()) == goallist[j].Id)
                    {
                        res += goallist[j].name + ",";
                    }
                }
            }
            if (res.Length > 0)
            {
                res = res.Remove(res.Length - 1);
            }
            return res;
        }
        public string ConvertToGoalIds(List<Goal> goals)
        {
            string res = string.Empty;
            for (int i = 0; i < goals.Count; i++)
            {
                res += goals[i].Id;
            }
            return res;
        }
        public List<Goal> GetGoals()
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT * FROM goals";
            adapter = new MySqlDataAdapter(sqlQuery, connection);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            List<Goal> res = new List<Goal>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Goal goal = new Goal(int.Parse(dt.Rows[i].ItemArray[0].ToString()),
                    dt.Rows[i].ItemArray[1].ToString(),
                    dt.Rows[i].ItemArray[2].ToString(),
                    int.Parse(dt.Rows[i].ItemArray[3].ToString()));
                res.Add(goal);
            }
            connection.Close();
            return res;
        }

        public int CreateWorkGroup(WorkGroup wg)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "INSERT INTO WorkGroups(name, goals, comments, workers) VALUES (\"" + wg.name + "\",\"" + ConvertToGoalIds(wg.goals) + "\",\"" + wg.comments + "\",\"" + ConvertToWorkersIds(wg.workers) + "\")";
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            connection.Open();
            sqlQuery = "SELECT MAX(id) FROM WorkGroups";
            cmd = new MySqlCommand(sqlQuery, connection);
            int res = Convert.ToInt32(cmd.ExecuteScalar());
            for (int i = 0; i < wg.workers.Count; i++)
            {
                wg.workers[i].workgroups += res;
                UpdateWorker(wg.workers[i]);
            }
            CreateChat(res);
            connection.Close();
            return res;
        }
        public void UpdateWorkGroup(int id, List<Worker> workers, string name, List<Goal> goals, string comments)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            string wk = ConvertToWorkersIds(workers);
            sqlQuery = "UPDATE WorkGroups SET name=\"" + name + "\",goals=\"" + ConvertToGoalIds(goals) + "\",comments=\"" + comments + "\",workers=\"" + wk + "\" WHERE id = " + id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void DeleteWorkGroup(WorkGroup wg)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "DELETE FROM WorkGroups WHERE id = " + wg.Id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            for (int i = 0; i < wg.workers.Count; i++)
            {
                wg.workers[i].workgroups = wg.workers[i].workgroups.Replace(wg.Id.ToString(), "");
                UpdateWorker(wg.workers[i]);
            }
            DeleteChat(wg.Id);
            connection.Close();
        }

        public void UpdateWorker(Worker worker)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            string wk = string.Empty;
            sqlQuery = "UPDATE Workers SET id_dolgnost = " + worker.dolgnost + ", name = \"" + worker.name + "\", birthdate = " + worker.bithdate + ", phone = \"" + worker.phone + "\", email = \"" + worker.email + "\", password = \"" + worker.password + "\", social = \"" + worker.social + "\", rating = " + worker.rating + ", id_skills = " + worker.id_skills + ", worktime = \"" + worker.worktime + "\", workgroups = \"" + worker.workgroups + "\", online = " + worker.online + ", rukovoditel = " + worker.rukovoditel + " WHERE id = " + worker.Id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
        }

        public void CreateGoal(Goal g)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "INSERT INTO goals(name, description, status) VALUES (\""+ g.name+"\",\""+g.description+"\","+g.status+")";
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void DeleteGoal(int id)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "DELETE FROM goals WHERE id = " + id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void UpdateGoal(int id, string name, string desc, int status)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "UPDATE goals SET name=\"" + name + "\",description=\"" + desc + "\",status=" + status + " WHERE id = " + id;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        
        public string SelectChat(int workgroup)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "SELECT text FROM chat WHERE workgroup = " + workgroup;
            cmd = new MySqlCommand(sqlQuery, connection);
            string res = cmd.ExecuteScalar().ToString();
            connection.Close();
            return res;
        }
        public void CreateChat(int workgroup)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "INSERT INTO chat(workgroup, text) VALUES (" + workgroup + ", \"\")";
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void DeleteChat(int workgroup)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "DELETE FROM chat WHERE workgroup = " + workgroup;
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
        public void SendMessage(int workgroup, string text)
        {
            connection = new MySqlConnection(connectionString);
            connection.Open();
            sqlQuery = "UPDATE chat SET text=CONCAT(text, \""+text+"\") WHERE 1";
            cmd = new MySqlCommand(sqlQuery, connection);
            cmd.ExecuteNonQuery();
            connection.Close();
        }
    }
}

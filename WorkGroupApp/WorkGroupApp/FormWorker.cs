﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkGroupApp
{
    public partial class FormWorker : Form
    {
        Connector con;
        FormLogin fl;
        Worker worker;
        List<Worker> workers;
        List<WorkGroup> workgroups;
        List<Goal> goals;
        public FormWorker(Worker _worker, FormLogin _fl)
        {
            InitializeComponent();
            worker = _worker;
            fl = _fl;
        }

        private void FormWorker_Load(object sender, EventArgs e)
        {
            comboBoxWorkGroups.Items.Clear();
            comboBoxWorkGruopsChat.Items.Clear();
            comboBoxSkills.Items.Clear();
            comboBoxRukovoditel.Items.Clear();
            con = new Connector();
            workers = con.GetWorkers();
            workgroups = con.GetWorkGroups();
            goals = con.GetGoals();
            for (int i = 0; i < workgroups.Count; i++)
            {
                for (int j = 0; j < worker.workgroups.Length; j++)
                {
                    if (int.Parse(worker.workgroups[j].ToString()) == workgroups[i].Id)
                    {
                        comboBoxWorkGroups.Items.Add(workgroups[i].name);
                        comboBoxWorkGruopsChat.Items.Add(workgroups[i].name);
                    }
                }
            }
            List<Sprav> skills = con.GetAllSkills();
            for (int i = 0; i < skills.Count; i++)
            {
                comboBoxSkills.Items.Add(skills[i].name);
                if (skills[i].id == worker.id_skills)
                {
                    comboBoxSkills.SelectedIndex = comboBoxSkills.Items.Count - 1;
                }
            }
            textBoxName.Text = worker.name;
            textBoxPhone.Text = worker.phone;
            textBoxEmail.Text = worker.email;
            textBoxPassword.Text = worker.password;
            textBoxSocial.Text = worker.social;
            textBoxWorkTime.Text = worker.worktime;
            dateTimePickerBirthday.Value = Convert.ToDateTime(con.ConvertToDate(worker.bithdate));
            for (int i = 0; i < workers.Count; i++)
            {
                if (workers[i].dolgnost == 1)
                {
                    comboBoxRukovoditel.Items.Add(workers[i].name);
                    if (workers[i].Id == worker.rukovoditel)
                    {
                        comboBoxRukovoditel.SelectedIndex = comboBoxRukovoditel.Items.Count - 1;
                    }
                }
            }
            this.Text = "Пользователь " + worker.name;
        }

        private void buttonDownloadAllWorkGroups_Click(object sender, EventArgs e)
        {
            workgroups = con.GetWorkGroups();
            workers = con.GetWorkers();
            goals = con.GetGoals();

            DataTable dt = new DataTable();
            for (int i = 0; i < 5; i++)
            {
                dt.Columns.Add(new DataColumn());
            }
            for (int i = 0; i < worker.workgroups.Length; i++)
            {
                for (int j = 0; j < workgroups.Count; j++)
                {
                    if (int.Parse(worker.workgroups[i].ToString()) == workgroups[j].Id)
                    {
                        string[] row = new string[dt.Columns.Count];
                        row[0] = workgroups[j].Id.ToString();
                        row[1] = workgroups[j].name;
                        row[2] = workgroups[j].GetGoalNames();
                        row[3] = workgroups[j].comments;
                        row[4] = String.Empty;
                        for (int l = 0; l < workgroups[j].workers.Count; l++)
                        {
                            row[4] += workgroups[j].workers[l].name + ",";
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            dataGridViewWorkGroups.DataSource = dt;
            dataGridViewWorkGroups.Columns[0].HeaderText = "Id";
            dataGridViewWorkGroups.Columns[1].HeaderText = "Имя";
            dataGridViewWorkGroups.Columns[2].HeaderText = "Цели";
            dataGridViewWorkGroups.Columns[3].HeaderText = "Комментарии";
            dataGridViewWorkGroups.Columns[4].HeaderText = "Работники";
        }

        private void FormWorker_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.SetOnline(worker, 0);
            fl.Show();
        }

        private void buttonDownloadAllGoals_Click(object sender, EventArgs e)
        {
            workgroups = con.GetWorkGroups();
            workers = con.GetWorkers();
            goals = con.GetGoals();

            comboBoxWorkGroups.Items.Clear();
            for (int i = 0; i < worker.workgroups.Length; i++)
            {
                for (int j = 0; j < workgroups.Count; j++)
                {
                    if (int.Parse(worker.workgroups[i].ToString()) == workgroups[j].Id)
                    {
                        comboBoxWorkGroups.Items.Add(workgroups[j].name);
                    }
                }
            }
        }

        private void comboBoxWorkGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 4; i++)
            {
                dt.Columns.Add(new DataColumn());
            }
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (workgroups[i].name == comboBoxWorkGroups.Text)
                {
                    for (int j = 0; j < workgroups[i].goals.Count; j++)
                    {
                        string[] row = new string[dt.Columns.Count];
                        row[0] = workgroups[i].goals[j].Id.ToString();
                        row[1] = workgroups[i].goals[j].name;
                        row[2] = workgroups[i].goals[j].description;
                        switch (workgroups[i].goals[j].status)
                        {
                            case 1:
                                row[3] = "Создана";
                                break;
                            case 2:
                                row[3] = "В работе";
                                break;
                            case 3:
                                row[3] = "Выполнена";
                                break;
                            default:
                                row[3] = "Ошибка";
                                break;
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            dataGridViewGoals.DataSource = dt;
            dataGridViewGoals.Columns[0].HeaderText = "Id";
            dataGridViewGoals.Columns[1].HeaderText = "Имя";
            dataGridViewGoals.Columns[2].HeaderText = "Описание";
            dataGridViewGoals.Columns[3].HeaderText = "Статус";
        }

        private void dataGridViewGoals_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dataGridViewGoals.SelectedCells[0].RowIndex;
            int index = int.Parse(dataGridViewGoals.Rows[rowindex].Cells[0].Value.ToString());
            Goal g = new Goal();
            for (int i = 0; i < goals.Count; i++)
            {
                if (index == goals[i].Id)
                {
                    g = goals[i];
                    break;
                }
            }
            comboBoxGoalStatus.SelectedIndex = g.status - 1;
        }

        private void buttonUpdateGoal_Click(object sender, EventArgs e)
        {
            if (comboBoxGoalStatus.Text != "")
            {
                int rowindex = dataGridViewGoals.SelectedCells[0].RowIndex;
                int index = int.Parse(dataGridViewGoals.Rows[rowindex].Cells[0].Value.ToString());
                Goal g = new Goal();
                for (int i = 0; i < goals.Count; i++)
                {
                    if (goals[i].Id == index)
                    {
                        con.UpdateGoal(index, goals[i].name, goals[i].description, comboBoxGoalStatus.SelectedIndex + 1);
                        g = goals[i];
                        break;
                    }
                }
                goals = con.GetGoals();
                for (int i = 0; i < workgroups.Count; i++)
                {
                    for (int j = 0; j < workgroups[i].goals.Count; j++)
                    {
                        if (workgroups[i].goals[j].Id == index)
                        {
                            workgroups[i].goals[j] = new Goal(index, g.name, g.description, comboBoxGoalStatus.SelectedIndex + 1);
                            con.UpdateWorkGroup(workgroups[i].Id, workgroups[i].workers, workgroups[i].name, workgroups[i].goals, workgroups[i].comments);
                            con.SendMessage(workgroups[i].Id, "\nИзменен статус задачи " + g.name + ": " + comboBoxGoalStatus.Text);
                            break;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите статус");
            }
        }

        private void buttonNapominanie_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("taskschd.msc");
        }

        private void comboBoxWorkGruopsChat_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBoxChat.Clear();
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (comboBoxWorkGruopsChat.Text == workgroups[i].name)
                {
                    richTextBoxChat.Text += con.SelectChat(workgroups[i].Id);
                    break;
                }
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text != string.Empty)
            {
                for (int i = 0; i < workgroups.Count; i++)
                {
                    if (comboBoxWorkGruopsChat.Text == workgroups[i].name)
                    {
                        con.SendMessage(comboBoxWorkGruopsChat.SelectedIndex, "\n" + worker.name + ": " + textBoxMessage.Text);
                        textBoxMessage.Clear();
                        comboBoxWorkGruopsChat_SelectedIndexChanged(null, null);
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Введите сообщение");
            }
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text != "" && textBoxEmail.Text != "" && textBoxPassword.Text != "" && comboBoxRukovoditel.Text != "" && comboBoxSkills.Text != "")
            {
                bool canChange = true;
                int ruk = worker.rukovoditel;
                for (int i = 0; i < workgroups.Count; i++)
                {
                    if (workgroups[i].workers.Contains(worker))
                    {
                        for (int j = 0; j < workgroups[i].goals.Count; j++)
                        {
                            if (workgroups[i].goals[j].status != 3)
                            {
                                canChange = false;
                                break;
                            }
                        }
                    }
                }
                if (canChange == true)
                {
                    for (int i = 0; i < workers.Count; i++)
                    {
                        if (workers[i].name == comboBoxRukovoditel.Text)
                        {
                            ruk = workers[i].Id;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Не все задачи выполнены. руководитель не будет изменен");
                }
                List<Sprav> skills = con.GetAllSkills();
                for (int i = 0; i < skills.Count; i++)
                {
                    if (skills[i].name == comboBoxSkills.Text)
                    {
                        con.UpdateWorker(new Worker(worker.Id, worker.dolgnost, textBoxName.Text, con.ConvertToUnixTime(dateTimePickerBirthday.Value), textBoxPhone.Text, textBoxEmail.Text, textBoxPassword.Text, textBoxSocial.Text, worker.rating, skills[i].id, textBoxWorkTime.Text, worker.workgroups, worker.online, ruk));
                        break;
                    }
                }
                workers = con.GetWorkers();
                for (int i = 0; i < workers.Count; i++)
                {
                    if (workers[i].Id == worker.Id)
                    {
                        worker = workers[i];
                        break;
                    }
                }
                FormWorker_Load(null, null);
            }
            else
            {
                MessageBox.Show("Не все необходимые данные введены");
            }
        }
    }
}

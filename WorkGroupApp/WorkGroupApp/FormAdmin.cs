﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkGroupApp
{
    public partial class FormAdmin : Form
    {
        Connector con;
        FormLogin fl;
        Worker worker;
        List<Worker> workers;
        List<WorkGroup> workgroups;
        List<Goal> goals;
        public FormAdmin(Worker _worker, FormLogin _fl)
        {
            InitializeComponent();
            worker = _worker;
            fl = _fl;
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            comboBoxWorkGroups.Items.Clear();
            comboBoxWorkGruopsChat.Items.Clear();
            comboBoxSkills.Items.Clear();
            con = new Connector();
            workers = con.GetWorkers();
            workgroups = con.GetWorkGroups();
            goals = con.GetGoals();
            for (int i = 0; i < workgroups.Count; i++)
            {
                for (int j = 0; j < worker.workgroups.Length; j++)
                {
                    if (int.Parse(worker.workgroups[j].ToString()) == workgroups[i].Id)
                    {
                        comboBoxWorkGroups.Items.Add(workgroups[i].name);
                        comboBoxWorkGruopsChat.Items.Add(workgroups[i].name);
                    }
                }
            }
            List<Sprav> skills = con.GetAllSkills();
            for (int i = 0; i < skills.Count; i++)
            {
                comboBoxSkills.Items.Add(skills[i].name);
                if (skills[i].id == worker.id_skills)
                {
                    comboBoxSkills.SelectedIndex = comboBoxSkills.Items.Count - 1;
                }
            }
            textBoxName.Text = worker.name;
            textBoxPhone.Text = worker.phone;
            textBoxEmail.Text = worker.email;
            textBoxPassword.Text = worker.password;
            textBoxSocial.Text = worker.social;
            textBoxWorkTime.Text = worker.worktime;
            dateTimePickerBirthday.Value = Convert.ToDateTime(con.ConvertToDate(worker.bithdate));
            this.Text = "Пользователь " + worker.name;
        }

        private void FormAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            con.SetOnline(worker, 0);
            fl.Show();
        }

        private void comboBoxWorkGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonDeleteWorker_Click(object sender, EventArgs e)
        {
            int rowindex = dataGridViewWorkGroups.SelectedCells[0].RowIndex;
            int index = int.Parse(dataGridViewWorkGroups.Rows[rowindex].Cells[0].Value.ToString());
            WorkGroup wg = new WorkGroup();
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (index == workgroups[i].Id)
                {
                    wg = workgroups[i];
                    break;
                }
            }
            for (int i = 0; i < wg.workers.Count; i++)
            {
                if (wg.workers[i].name == comboBoxDeleteWorker.Text)
                {
                    wg.workers.RemoveAt(i);
                    workers[i].workgroups = workers[i].workgroups.Replace(wg.Id.ToString(), "");
                    con.UpdateWorkGroup(wg.Id, wg.workers, wg.name, wg.goals, wg.comments);
                    con.UpdateWorker(workers[i]);
                }
            }


            buttonDownloadAllWorkGroups_Click(null, null);
        }

        private void buttonAddWorker_Click(object sender, EventArgs e)
        {
            int rowindex = dataGridViewWorkGroups.SelectedCells[0].RowIndex;
            int index = int.Parse(dataGridViewWorkGroups.Rows[rowindex].Cells[0].Value.ToString());
            WorkGroup wg = new WorkGroup();
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (index == workgroups[i].Id)
                {
                    wg = workgroups[i];
                    break;
                }
            }
            string index1 = comboBoxWorkers.Text;
            for (int i = 0; i < workers.Count; i++)
            {
                if (workers[i].name == index1)
                {
                    for (int j = 0; j < wg.workers.Count; j++)
                    {
                        if (workers[i].Id != wg.workers[j].Id)
                        {
                            wg.workers.Add(workers[i]);
                            workers[i].workgroups += wg.Id;
                            con.UpdateWorker(workers[i]);
                        }
                        con.UpdateWorkGroup(wg.Id, wg.workers, wg.name, wg.goals, wg.comments);
                    }
                }
            }
            buttonDownloadAllWorkGroups_Click(null, null);
        }

        private void buttonDownloadAllWorkGroups_Click(object sender, EventArgs e)
        {
            workgroups = con.GetWorkGroups();
            workers = con.GetWorkers();
            goals = con.GetGoals();

            comboBoxDeleteWorker.Items.Clear();
            comboBoxWorkers.Items.Clear();

            DataTable dt = new DataTable();
            for (int i = 0; i < 5; i++)
            {
                dt.Columns.Add(new DataColumn());
            }
            for (int i = 0; i < worker.workgroups.Length; i++)
            {
                for (int j = 0; j < workgroups.Count; j++)
                {
                    if (int.Parse(worker.workgroups[i].ToString()) == workgroups[j].Id)
                    {
                        string[] row = new string[dt.Columns.Count];
                        row[0] = workgroups[j].Id.ToString();
                        row[1] = workgroups[j].name;
                        row[2] = workgroups[j].GetGoalNames();
                        row[3] = workgroups[j].comments;
                        row[4] = String.Empty;
                        for (int l = 0; l < workgroups[j].workers.Count; l++)
                        {
                            row[4] += workgroups[j].workers[l].name + ",";
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            dataGridViewWorkGroups.DataSource = dt;
            dataGridViewWorkGroups.Columns[0].HeaderText = "Id";
            dataGridViewWorkGroups.Columns[1].HeaderText = "Имя";
            dataGridViewWorkGroups.Columns[2].HeaderText = "Цели";
            dataGridViewWorkGroups.Columns[3].HeaderText = "Комментарии";
            dataGridViewWorkGroups.Columns[4].HeaderText = "Работники";
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxWorkGroupName.Clear();
            richTextBoxComments.Clear();
            textBoxGoalName.Clear();
        }

        private void buttonCreateWorkGroup_Click(object sender, EventArgs e)
        {
            if (textBoxWorkGroupName.Text != "")
            {
                WorkGroup wg = new WorkGroup(0, new List<Worker>() { worker }, textBoxWorkGroupName.Text, new List<Goal>(), richTextBoxComments.Text);
                con.CreateWorkGroup(wg);
                buttonDownloadAllWorkGroups_Click(null, null);
            }
            else
            {
                MessageBox.Show("Введены не все данные");
            }
        }

        private void buttonUpdateWorkGroup_Click(object sender, EventArgs e)
        {
            if (textBoxWorkGroupName.Text != "" && dataGridViewWorkGroups.SelectedCells.Count > 0)
            {
                int rowindex = dataGridViewWorkGroups.SelectedCells[0].RowIndex;
                int index = int.Parse(dataGridViewWorkGroups.Rows[rowindex].Cells[0].Value.ToString());
                WorkGroup wg = new WorkGroup();
                for (int i = 0; i < workgroups.Count; i++)
                {
                    if (index == workgroups[i].Id)
                    {
                        wg = workgroups[i];
                        break;
                    }
                }
                con.UpdateWorkGroup(index, wg.workers, textBoxWorkGroupName.Text, wg.goals, richTextBoxComments.Text);
                buttonDownloadAllWorkGroups_Click(null, null);
            }
            else
            {
                MessageBox.Show("Введены не все данные или не выбран объект");
            }
        }

        private void dataGridViewWorkGroups_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBoxWorkGroupName.Clear();
            richTextBoxComments.Clear();
            int rowindex = dataGridViewWorkGroups.SelectedCells[0].RowIndex;
            int index = int.Parse(dataGridViewWorkGroups.Rows[rowindex].Cells[0].Value.ToString());
            comboBoxDeleteWorker.Items.Clear();
            WorkGroup wg = new WorkGroup();
            
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (index == workgroups[i].Id)
                {
                    wg = workgroups[i];
                    textBoxWorkGroupName.Text += workgroups[i].Name;
                    richTextBoxComments.Text += workgroups[i].comments;
                    break;
                }
            }
            for (int i = 0; i < workers.Count; i++)
            {
                if (workers[i].rukovoditel == worker.Id)
                {
                    if (workers[i].workgroups.Length != 0)
                    {
                        for (int j = 0; j < workers[i].workgroups.Length; j++)
                        {
                            if (int.Parse(workers[i].workgroups[j].ToString()) == wg.Id && workers[i].Id != worker.Id)
                            {
                                comboBoxDeleteWorker.Items.Add(workers[i].name);
                            }
                            else
                            {
                                comboBoxWorkers.Items.Add(workers[i].name);
                            }
                        }
                    }
                    else
                    {
                        comboBoxWorkers.Items.Add(workers[i].name);
                    }
                }
            }
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
        
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = dataGridViewGoals.SelectedCells[0].RowIndex;
            int index = int.Parse(dataGridViewGoals.Rows[rowindex].Cells[0].Value.ToString());
            Goal g = new Goal();
            for (int i = 0; i < goals.Count; i++)
            {
                if (index == goals[i].Id)
                {
                    g = goals[i];
                    break;
                }
            }
            textBoxGoalName.Clear();
            richTextBoxDesc.Clear();
            textBoxGoalName.Text += g.name;
            richTextBoxDesc.Text += g.description;
            comboBoxGoalStatus.SelectedIndex = g.status-1;
        }

        private void buttonAddGoal_Click(object sender, EventArgs e)
        {
            if (textBoxGoalName.Text != "" && comboBoxGoalStatus.Text != "" && comboBoxWorkGroups.Text != "")
            {
                {
                    Goal g = new Goal(0, textBoxGoalName.Text, richTextBoxDesc.Text, comboBoxGoalStatus.SelectedIndex + 1);
                    con.CreateGoal(g);
                    goals = con.GetGoals();
                    g = goals[goals.Count - 1];
                    for (int i = 0; i < workgroups.Count; i++)
                    {
                        if (workgroups[i].name == comboBoxWorkGroups.Text)
                        {
                            workgroups[i].goals.Add(g);
                            con.UpdateWorkGroup(workgroups[i].Id, workgroups[i].workers, workgroups[i].name, workgroups[i].goals, workgroups[i].comments);
                            workgroups = con.GetWorkGroups();
                            con.SendMessage(workgroups[i].Id - 1, "\nСоздана задача: " + g.name);
                            break;
                        }
                    }
                }
            }
        }

        private void buttonUpdateWorkers_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 11; i++)
            {
                dt.Columns.Add(new DataColumn());
            }
            for (int i = 0; i < workers.Count; i++)
            {
                if (workers[i].rukovoditel == worker.Id)
                {
                    string[] row = new string[dt.Columns.Count];
                    row[0] = workers[i].Id.ToString();
                    row[1] = con.GetDolgnost(workers[i].dolgnost);
                    row[2] = workers[i].name;
                    row[3] = con.ConvertToDate(workers[i].bithdate);
                    row[4] = workers[i].phone;
                    row[5] = workers[i].email;
                    row[6] = workers[i].social;
                    row[7] = con.GetSkill(workers[i].id_skills);
                    row[8] = workers[i].worktime;
                    row[9] = con.ConvertToWorkGroupsNames(workers[i].workgroups);
                    if (workers[i].online == 0)
                    {
                        row[10] = "Нет";
                    }
                    else
                    {
                        row[10] = "Да";
                    }
                    dt.Rows.Add(row);
                }
            }
            dataGridViewWorkers.DataSource = dt;
            dataGridViewWorkers.Columns[0].HeaderText = "Id";
            dataGridViewWorkers.Columns[1].HeaderText = "Должность";
            dataGridViewWorkers.Columns[2].HeaderText = "Имя";
            dataGridViewWorkers.Columns[3].HeaderText = "Дата рождения";
            dataGridViewWorkers.Columns[4].HeaderText = "Телефон";
            dataGridViewWorkers.Columns[5].HeaderText = "Электронная почта";
            dataGridViewWorkers.Columns[6].HeaderText = "Адрес в соцсети";
            dataGridViewWorkers.Columns[7].HeaderText = "Навыки";
            dataGridViewWorkers.Columns[8].HeaderText = "Рабочий график";
            dataGridViewWorkers.Columns[9].HeaderText = "Рабочие группы";
            dataGridViewWorkers.Columns[10].HeaderText = "На работе";
        }

        private void buttonDeleteWorkGroup_Click(object sender, EventArgs e)
        {
            if (dataGridViewWorkGroups.SelectedCells.Count > 0)
            {
                int rowindex = dataGridViewWorkGroups.SelectedCells[0].RowIndex;
                int index = int.Parse(dataGridViewWorkGroups.Rows[rowindex].Cells[0].Value.ToString());
                WorkGroup wg = new WorkGroup();
                for (int i = 0; i < workgroups.Count; i++)
                {
                    if (workgroups[i].Id == index)
                    {
                        wg = workgroups[i];
                        break;
                    }
                }
                con.DeleteWorkGroup(wg);
                buttonDownloadAllWorkGroups_Click(null, null);
            }
            else
            {
                MessageBox.Show("Не выбран объект");
            }
        }

        private void buttonDownloadAllGoals_Click(object sender, EventArgs e)
        {
            workgroups = con.GetWorkGroups();
            workers = con.GetWorkers();
            goals = con.GetGoals();

            comboBoxWorkGroups.Items.Clear();
            for (int i = 0; i < worker.workgroups.Length; i++)
            {
                for (int j = 0; j < workgroups.Count; j++)
                {
                    if (int.Parse(worker.workgroups[i].ToString()) == workgroups[j].Id)
                    {
                        comboBoxWorkGroups.Items.Add(workgroups[j].name);
                    }
                }
            }
        }

        private void comboBoxWorkGroups_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 4; i++)
            {
                dt.Columns.Add(new DataColumn());
            }
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (workgroups[i].name == comboBoxWorkGroups.Text)
                {
                    for (int j = 0; j < workgroups[i].goals.Count; j++)
                    {
                        string[] row = new string[dt.Columns.Count];
                        row[0] = workgroups[i].goals[j].Id.ToString();
                        row[1] = workgroups[i].goals[j].name;
                        row[2] = workgroups[i].goals[j].description;
                        switch (workgroups[i].goals[j].status)
                        {
                            case 1:
                                row[3] = "Создана";
                                break;
                            case 2:
                                row[3] = "В работе";
                                break;
                            case 3:
                                row[3] = "Выполнена";
                                break;
                            default:
                                row[3] = "Ошибка";
                                break;
                        }
                        dt.Rows.Add(row);
                    }
                }
            }
            dataGridViewGoals.DataSource = dt;
            dataGridViewGoals.Columns[0].HeaderText = "Id";
            dataGridViewGoals.Columns[1].HeaderText = "Имя";
            dataGridViewGoals.Columns[2].HeaderText = "Описание";
            dataGridViewGoals.Columns[3].HeaderText = "Статус";
        }

        private void buttonDeleteGoal_Click(object sender, EventArgs e)
        {
            if (dataGridViewWorkGroups.SelectedCells.Count > 0)
            {
                int rowindex = dataGridViewGoals.SelectedCells[0].RowIndex;
                int index = int.Parse(dataGridViewGoals.Rows[rowindex].Cells[0].Value.ToString());
                con.DeleteGoal(index);
                for (int i = 0; i < workgroups.Count; i++)
                {
                    for (int j = 0; j < workgroups[i].goals.Count; j++)
                    {
                        if (workgroups[i].goals[j].Id == index)
                        {
                            workgroups[i].goals.RemoveAt(j);
                            con.UpdateWorkGroup(workgroups[i].Id, workgroups[i].workers, workgroups[i].name, workgroups[i].goals, workgroups[i].comments);
                            break;
                        }
                    }
                }
                workgroups = con.GetWorkGroups();
                goals = con.GetGoals();
            }
            else
            {
                MessageBox.Show("Не выбран объект для удаления");
            }
        }

        private void buttonUpdateGoal_Click(object sender, EventArgs e)
        {
            if (textBoxGoalName.Text != "" && comboBoxGoalStatus.Text != "" && dataGridViewWorkGroups.SelectedCells.Count > 0)
            {
                int rowindex = dataGridViewGoals.SelectedCells[0].RowIndex;
                int index = int.Parse(dataGridViewGoals.Rows[rowindex].Cells[0].Value.ToString());
                con.UpdateGoal(index, textBoxGoalName.Text, richTextBoxDesc.Text, comboBoxGoalStatus.SelectedIndex + 1);
                goals = con.GetGoals();
                for (int i = 0; i < workgroups.Count; i++)
                {
                    for (int j = 0; j < workgroups[i].goals.Count; j++)
                    {
                        if (workgroups[i].goals[j].Id == index)
                        {
                            workgroups[i].goals[j] = new Goal(index, textBoxGoalName.Text, richTextBoxDesc.Text, comboBoxGoalStatus.SelectedIndex + 1);
                            con.UpdateWorkGroup(workgroups[i].Id, workgroups[i].workers, workgroups[i].name, workgroups[i].goals, workgroups[i].comments);
                            break;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Не указаны все необходимые данные или не выбран объект");
            }
        }

        private void comboBoxWorkGruopsChat_SelectedIndexChanged(object sender, EventArgs e)
        {
            richTextBoxChat.Clear();
            for (int i = 0; i < workgroups.Count; i++)
            {
                if (comboBoxWorkGruopsChat.Text == workgroups[i].name)
                {
                    richTextBoxChat.Text += con.SelectChat(workgroups[i].Id);
                    break;
                }
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text != "")
            {
                for (int i = 0; i < workgroups.Count; i++)
                {
                    if (comboBoxWorkGruopsChat.Text == workgroups[i].name)
                    {
                        con.SendMessage(comboBoxWorkGruopsChat.SelectedIndex, "\n" + worker.name + ": " + textBoxMessage.Text);
                        textBoxMessage.Clear();
                        comboBoxWorkGruopsChat_SelectedIndexChanged(null, null);
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Введите ссобщение");
            }
        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text != "" && textBoxEmail.Text != "" && textBoxPassword.Text != "" && comboBoxSkills.Text != "")
            {
                List<Sprav> skills = con.GetAllSkills();
                for (int i = 0; i < skills.Count; i++)
                {
                    if (skills[i].name == comboBoxSkills.Text)
                    {
                        con.UpdateWorker(new Worker(worker.Id, worker.dolgnost, textBoxName.Text, con.ConvertToUnixTime(dateTimePickerBirthday.Value), textBoxPhone.Text, textBoxEmail.Text, textBoxPassword.Text, textBoxSocial.Text, worker.rating, skills[i].id, textBoxWorkTime.Text, worker.workgroups, worker.online, worker.rukovoditel));
                        break;
                    }
                }
                workers = con.GetWorkers();
                for (int i = 0; i < workers.Count; i++)
                {
                    if (workers[i].Id == worker.Id)
                    {
                        worker = workers[i];
                        break;
                    }
                }
                FormAdmin_Load(null, null);
            }
            else
            {
                MessageBox.Show("Не все данные введены");
            }
        }

        private void buttonNapominanie_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("taskschd.msc");
        }
    }
}

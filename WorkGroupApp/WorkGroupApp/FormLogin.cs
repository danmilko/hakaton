﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkGroupApp
{
    public partial class FormLogin : Form
    {
        Connector con;
        public FormLogin()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            int res = con.Login(textBoxLogin.Text, textBoxPassword.Text);
            if (res == 0)
            {
                MessageBox.Show("Неправильный логин или пароль");
            }
            else
            {
                List<Worker> workers = new List<Worker>();
                workers = con.GetWorkers();
                for (int i = 0; i < workers.Count; i++)
                {
                    if (workers[i].Id == res)
                    {
                        con.SetOnline(workers[i], 1);
                        if (workers[i].Dolgnost == 1)
                        {
                            FormAdmin fa = new FormAdmin(workers[i], this);
                            this.Hide();
                            fa.Show();
                        }
                        else
                        {
                            FormWorker fw = new FormWorker(workers[i], this);
                            this.Hide();
                            fw.Show();
                        }
                    }
                }
            }
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            con = new Connector();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkGroupApp
{
    public class Worker
    {
        private int id;
        public int dolgnost;
        public string name;
        public int bithdate;
        public string phone;
        public string email;
        public string password;
        public string social;
        public int rating;
        public int id_skills;
        public string worktime;
        public string workgroups;
        public int online;
        public int rukovoditel;
        public int Id
        {
            get
            {
                return id;
            }
        }
        public int Dolgnost
        {
            get
            {
                return dolgnost;
            }
        }
        public Worker(int _id, 
            int _dolgnost,
            string _name,
            int _bithdate,
            string _phone,
            string _email,
            string _password,
            string _social,
            int _rating,
            int _id_skills,
            string _worktime,
            string _workgroups,
            int _online,
            int _rukovoditel)
        {
            id = _id;
            dolgnost = _dolgnost;
            name = _name;
            bithdate = _bithdate;
            phone = _phone;
            email = _email;
            password = _password;
            social = _social;
            rating = _rating;
            id_skills = _id_skills;
            worktime = _worktime;
            workgroups = _workgroups;
            online = _online;
            rukovoditel = _rukovoditel;
        }
        public Worker(int _dolgnost,
            string _name,
            int _bithdate,
            string _phone,
            string _email,
            string _password,
            string _social)
        {
            dolgnost = _dolgnost;
            name = _name;
            bithdate = _bithdate;
            phone = _phone;
            email = _email;
            password = _password;
            social = _social;
            rating = 1;
            online = 0;
        }
        public void Edit(string res, int index)
        {
            switch (index)
            {
                case 1:
                    dolgnost = int.Parse(res);
                    break;
                case 2:
                    name = res;
                    break;
                case 3:
                    bithdate = int.Parse(res);
                    break;
                case 4:
                    phone = res;
                    break;
                case 5:
                    email = res;
                    break;
                case 6:
                    social = res;
                    break;
                case 7:
                    rating = int.Parse(res);
                    break;
                default:
                    break;
            }
        }
    }
}

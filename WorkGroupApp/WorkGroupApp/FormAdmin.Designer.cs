﻿namespace WorkGroupApp
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewWorkers = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonUpdateWorkers = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonAddWorker = new System.Windows.Forms.Button();
            this.buttonDeleteWorker = new System.Windows.Forms.Button();
            this.comboBoxDeleteWorker = new System.Windows.Forms.ComboBox();
            this.comboBoxWorkers = new System.Windows.Forms.ComboBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonDeleteWorkGroup = new System.Windows.Forms.Button();
            this.buttonUpdateWorkGroup = new System.Windows.Forms.Button();
            this.buttonCreateWorkGroup = new System.Windows.Forms.Button();
            this.richTextBoxComments = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxWorkGroupName = new System.Windows.Forms.TextBox();
            this.buttonDownloadAllWorkGroups = new System.Windows.Forms.Button();
            this.dataGridViewWorkGroups = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonDownloadAllGoals = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBoxDesc = new System.Windows.Forms.RichTextBox();
            this.buttonUpdateGoal = new System.Windows.Forms.Button();
            this.buttonDeleteGoal = new System.Windows.Forms.Button();
            this.buttonAddGoal = new System.Windows.Forms.Button();
            this.textBoxGoalName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxGoalStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewGoals = new System.Windows.Forms.DataGridView();
            this.comboBoxWorkGroups = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.richTextBoxChat = new System.Windows.Forms.RichTextBox();
            this.comboBoxWorkGruopsChat = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.buttonSend = new System.Windows.Forms.Button();
            this.buttonUpdateChat = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxSocial = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxWorkTime = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimePickerBirthday = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonNapominanie = new System.Windows.Forms.Button();
            this.comboBoxSkills = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkers)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkGroups)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGoals)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewWorkers
            // 
            this.dataGridViewWorkers.AllowUserToAddRows = false;
            this.dataGridViewWorkers.AllowUserToDeleteRows = false;
            this.dataGridViewWorkers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewWorkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWorkers.Location = new System.Drawing.Point(6, 28);
            this.dataGridViewWorkers.Name = "dataGridViewWorkers";
            this.dataGridViewWorkers.ReadOnly = true;
            this.dataGridViewWorkers.Size = new System.Drawing.Size(563, 310);
            this.dataGridViewWorkers.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(573, 371);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.buttonUpdateWorkers);
            this.tabPage1.Controls.Add(this.dataGridViewWorkers);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(565, 345);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Сотрудники в подчинении";
            // 
            // buttonUpdateWorkers
            // 
            this.buttonUpdateWorkers.Location = new System.Drawing.Point(7, 4);
            this.buttonUpdateWorkers.Name = "buttonUpdateWorkers";
            this.buttonUpdateWorkers.Size = new System.Drawing.Size(195, 23);
            this.buttonUpdateWorkers.TabIndex = 2;
            this.buttonUpdateWorkers.Text = "Обновить";
            this.buttonUpdateWorkers.UseVisualStyleBackColor = true;
            this.buttonUpdateWorkers.Click += new System.EventHandler(this.buttonUpdateWorkers_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.buttonAddWorker);
            this.tabPage2.Controls.Add(this.buttonDeleteWorker);
            this.tabPage2.Controls.Add(this.comboBoxDeleteWorker);
            this.tabPage2.Controls.Add(this.comboBoxWorkers);
            this.tabPage2.Controls.Add(this.buttonClear);
            this.tabPage2.Controls.Add(this.buttonDeleteWorkGroup);
            this.tabPage2.Controls.Add(this.buttonUpdateWorkGroup);
            this.tabPage2.Controls.Add(this.buttonCreateWorkGroup);
            this.tabPage2.Controls.Add(this.richTextBoxComments);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.textBoxWorkGroupName);
            this.tabPage2.Controls.Add(this.buttonDownloadAllWorkGroups);
            this.tabPage2.Controls.Add(this.dataGridViewWorkGroups);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(565, 345);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Рабочие группы";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // buttonAddWorker
            // 
            this.buttonAddWorker.Location = new System.Drawing.Point(403, 293);
            this.buttonAddWorker.Name = "buttonAddWorker";
            this.buttonAddWorker.Size = new System.Drawing.Size(156, 23);
            this.buttonAddWorker.TabIndex = 8;
            this.buttonAddWorker.Text = "Добавить сотрудника";
            this.buttonAddWorker.UseVisualStyleBackColor = true;
            this.buttonAddWorker.Click += new System.EventHandler(this.buttonAddWorker_Click);
            // 
            // buttonDeleteWorker
            // 
            this.buttonDeleteWorker.Location = new System.Drawing.Point(403, 208);
            this.buttonDeleteWorker.Name = "buttonDeleteWorker";
            this.buttonDeleteWorker.Size = new System.Drawing.Size(156, 23);
            this.buttonDeleteWorker.TabIndex = 7;
            this.buttonDeleteWorker.Text = "Удалить выбранного";
            this.buttonDeleteWorker.UseVisualStyleBackColor = true;
            this.buttonDeleteWorker.Click += new System.EventHandler(this.buttonDeleteWorker_Click);
            // 
            // comboBoxDeleteWorker
            // 
            this.comboBoxDeleteWorker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDeleteWorker.FormattingEnabled = true;
            this.comboBoxDeleteWorker.Location = new System.Drawing.Point(403, 237);
            this.comboBoxDeleteWorker.Name = "comboBoxDeleteWorker";
            this.comboBoxDeleteWorker.Size = new System.Drawing.Size(155, 21);
            this.comboBoxDeleteWorker.TabIndex = 6;
            // 
            // comboBoxWorkers
            // 
            this.comboBoxWorkers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxWorkers.FormattingEnabled = true;
            this.comboBoxWorkers.Location = new System.Drawing.Point(403, 322);
            this.comboBoxWorkers.Name = "comboBoxWorkers";
            this.comboBoxWorkers.Size = new System.Drawing.Size(155, 21);
            this.comboBoxWorkers.TabIndex = 6;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(3, 322);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(162, 23);
            this.buttonClear.TabIndex = 5;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonDeleteWorkGroup
            // 
            this.buttonDeleteWorkGroup.Location = new System.Drawing.Point(3, 293);
            this.buttonDeleteWorkGroup.Name = "buttonDeleteWorkGroup";
            this.buttonDeleteWorkGroup.Size = new System.Drawing.Size(162, 23);
            this.buttonDeleteWorkGroup.TabIndex = 5;
            this.buttonDeleteWorkGroup.Text = "Удалить";
            this.buttonDeleteWorkGroup.UseVisualStyleBackColor = true;
            this.buttonDeleteWorkGroup.Click += new System.EventHandler(this.buttonDeleteWorkGroup_Click);
            // 
            // buttonUpdateWorkGroup
            // 
            this.buttonUpdateWorkGroup.Location = new System.Drawing.Point(3, 264);
            this.buttonUpdateWorkGroup.Name = "buttonUpdateWorkGroup";
            this.buttonUpdateWorkGroup.Size = new System.Drawing.Size(162, 23);
            this.buttonUpdateWorkGroup.TabIndex = 5;
            this.buttonUpdateWorkGroup.Text = "Изменить";
            this.buttonUpdateWorkGroup.UseVisualStyleBackColor = true;
            this.buttonUpdateWorkGroup.Click += new System.EventHandler(this.buttonUpdateWorkGroup_Click);
            // 
            // buttonCreateWorkGroup
            // 
            this.buttonCreateWorkGroup.Location = new System.Drawing.Point(3, 235);
            this.buttonCreateWorkGroup.Name = "buttonCreateWorkGroup";
            this.buttonCreateWorkGroup.Size = new System.Drawing.Size(162, 23);
            this.buttonCreateWorkGroup.TabIndex = 5;
            this.buttonCreateWorkGroup.Text = "Создать";
            this.buttonCreateWorkGroup.UseVisualStyleBackColor = true;
            this.buttonCreateWorkGroup.Click += new System.EventHandler(this.buttonCreateWorkGroup_Click);
            // 
            // richTextBoxComments
            // 
            this.richTextBoxComments.Location = new System.Drawing.Point(174, 208);
            this.richTextBoxComments.Name = "richTextBoxComments";
            this.richTextBoxComments.Size = new System.Drawing.Size(223, 130);
            this.richTextBoxComments.TabIndex = 4;
            this.richTextBoxComments.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(171, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Комментарии";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 192);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Имя";
            // 
            // textBoxWorkGroupName
            // 
            this.textBoxWorkGroupName.Location = new System.Drawing.Point(3, 208);
            this.textBoxWorkGroupName.Name = "textBoxWorkGroupName";
            this.textBoxWorkGroupName.Size = new System.Drawing.Size(162, 20);
            this.textBoxWorkGroupName.TabIndex = 2;
            // 
            // buttonDownloadAllWorkGroups
            // 
            this.buttonDownloadAllWorkGroups.Location = new System.Drawing.Point(6, 6);
            this.buttonDownloadAllWorkGroups.Name = "buttonDownloadAllWorkGroups";
            this.buttonDownloadAllWorkGroups.Size = new System.Drawing.Size(248, 23);
            this.buttonDownloadAllWorkGroups.TabIndex = 1;
            this.buttonDownloadAllWorkGroups.Text = "Загрузить все рабочие группы";
            this.buttonDownloadAllWorkGroups.UseVisualStyleBackColor = true;
            this.buttonDownloadAllWorkGroups.Click += new System.EventHandler(this.buttonDownloadAllWorkGroups_Click);
            // 
            // dataGridViewWorkGroups
            // 
            this.dataGridViewWorkGroups.AllowUserToAddRows = false;
            this.dataGridViewWorkGroups.AllowUserToDeleteRows = false;
            this.dataGridViewWorkGroups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewWorkGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWorkGroups.Location = new System.Drawing.Point(3, 35);
            this.dataGridViewWorkGroups.Name = "dataGridViewWorkGroups";
            this.dataGridViewWorkGroups.ReadOnly = true;
            this.dataGridViewWorkGroups.Size = new System.Drawing.Size(552, 150);
            this.dataGridViewWorkGroups.TabIndex = 0;
            this.dataGridViewWorkGroups.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewWorkGroups_CellClick);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.buttonDownloadAllGoals);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.dataGridViewGoals);
            this.tabPage3.Controls.Add(this.comboBoxWorkGroups);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(565, 345);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Цели";
            // 
            // buttonDownloadAllGoals
            // 
            this.buttonDownloadAllGoals.Location = new System.Drawing.Point(7, 7);
            this.buttonDownloadAllGoals.Name = "buttonDownloadAllGoals";
            this.buttonDownloadAllGoals.Size = new System.Drawing.Size(165, 23);
            this.buttonDownloadAllGoals.TabIndex = 8;
            this.buttonDownloadAllGoals.Text = "Загрузить все задачи";
            this.buttonDownloadAllGoals.UseVisualStyleBackColor = true;
            this.buttonDownloadAllGoals.Click += new System.EventHandler(this.buttonDownloadAllGoals_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonNapominanie);
            this.groupBox1.Controls.Add(this.richTextBoxDesc);
            this.groupBox1.Controls.Add(this.buttonUpdateGoal);
            this.groupBox1.Controls.Add(this.buttonDeleteGoal);
            this.groupBox1.Controls.Add(this.buttonAddGoal);
            this.groupBox1.Controls.Add(this.textBoxGoalName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxGoalStatus);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(7, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(552, 175);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Цели";
            // 
            // richTextBoxDesc
            // 
            this.richTextBoxDesc.Location = new System.Drawing.Point(247, 32);
            this.richTextBoxDesc.Name = "richTextBoxDesc";
            this.richTextBoxDesc.Size = new System.Drawing.Size(299, 108);
            this.richTextBoxDesc.TabIndex = 5;
            this.richTextBoxDesc.Text = "";
            // 
            // buttonUpdateGoal
            // 
            this.buttonUpdateGoal.Location = new System.Drawing.Point(10, 88);
            this.buttonUpdateGoal.Name = "buttonUpdateGoal";
            this.buttonUpdateGoal.Size = new System.Drawing.Size(231, 23);
            this.buttonUpdateGoal.TabIndex = 4;
            this.buttonUpdateGoal.Text = "Изменить задачу";
            this.buttonUpdateGoal.UseVisualStyleBackColor = true;
            this.buttonUpdateGoal.Click += new System.EventHandler(this.buttonUpdateGoal_Click);
            // 
            // buttonDeleteGoal
            // 
            this.buttonDeleteGoal.Location = new System.Drawing.Point(10, 117);
            this.buttonDeleteGoal.Name = "buttonDeleteGoal";
            this.buttonDeleteGoal.Size = new System.Drawing.Size(231, 23);
            this.buttonDeleteGoal.TabIndex = 4;
            this.buttonDeleteGoal.Text = "Удалить задачу";
            this.buttonDeleteGoal.UseVisualStyleBackColor = true;
            this.buttonDeleteGoal.Click += new System.EventHandler(this.buttonDeleteGoal_Click);
            // 
            // buttonAddGoal
            // 
            this.buttonAddGoal.Location = new System.Drawing.Point(10, 146);
            this.buttonAddGoal.Name = "buttonAddGoal";
            this.buttonAddGoal.Size = new System.Drawing.Size(231, 23);
            this.buttonAddGoal.TabIndex = 4;
            this.buttonAddGoal.Text = "Добавить задачу";
            this.buttonAddGoal.UseVisualStyleBackColor = true;
            this.buttonAddGoal.Click += new System.EventHandler(this.buttonAddGoal_Click);
            // 
            // textBoxGoalName
            // 
            this.textBoxGoalName.Location = new System.Drawing.Point(41, 19);
            this.textBoxGoalName.Name = "textBoxGoalName";
            this.textBoxGoalName.Size = new System.Drawing.Size(197, 20);
            this.textBoxGoalName.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Статус";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(244, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Описание";
            // 
            // comboBoxGoalStatus
            // 
            this.comboBoxGoalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGoalStatus.FormattingEnabled = true;
            this.comboBoxGoalStatus.Items.AddRange(new object[] {
            "Создана",
            "В работе",
            "Выполнена"});
            this.comboBoxGoalStatus.Location = new System.Drawing.Point(53, 45);
            this.comboBoxGoalStatus.Name = "comboBoxGoalStatus";
            this.comboBoxGoalStatus.Size = new System.Drawing.Size(185, 21);
            this.comboBoxGoalStatus.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Имя";
            // 
            // dataGridViewGoals
            // 
            this.dataGridViewGoals.AllowUserToAddRows = false;
            this.dataGridViewGoals.AllowUserToDeleteRows = false;
            this.dataGridViewGoals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGoals.Location = new System.Drawing.Point(7, 33);
            this.dataGridViewGoals.Name = "dataGridViewGoals";
            this.dataGridViewGoals.ReadOnly = true;
            this.dataGridViewGoals.Size = new System.Drawing.Size(552, 124);
            this.dataGridViewGoals.TabIndex = 0;
            this.dataGridViewGoals.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // comboBoxWorkGroups
            // 
            this.comboBoxWorkGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxWorkGroups.FormattingEnabled = true;
            this.comboBoxWorkGroups.Location = new System.Drawing.Point(227, 6);
            this.comboBoxWorkGroups.Name = "comboBoxWorkGroups";
            this.comboBoxWorkGroups.Size = new System.Drawing.Size(185, 21);
            this.comboBoxWorkGroups.TabIndex = 1;
            this.comboBoxWorkGroups.SelectedIndexChanged += new System.EventHandler(this.comboBoxWorkGroups_SelectedIndexChanged_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(180, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Группа";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.buttonUpdateChat);
            this.tabPage4.Controls.Add(this.buttonSend);
            this.tabPage4.Controls.Add(this.textBoxMessage);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.comboBoxWorkGruopsChat);
            this.tabPage4.Controls.Add(this.richTextBoxChat);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(565, 345);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Чат";
            // 
            // richTextBoxChat
            // 
            this.richTextBoxChat.Location = new System.Drawing.Point(7, 34);
            this.richTextBoxChat.Name = "richTextBoxChat";
            this.richTextBoxChat.ReadOnly = true;
            this.richTextBoxChat.Size = new System.Drawing.Size(552, 278);
            this.richTextBoxChat.TabIndex = 0;
            this.richTextBoxChat.Text = "";
            // 
            // comboBoxWorkGruopsChat
            // 
            this.comboBoxWorkGruopsChat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxWorkGruopsChat.FormattingEnabled = true;
            this.comboBoxWorkGruopsChat.Location = new System.Drawing.Point(7, 4);
            this.comboBoxWorkGruopsChat.Name = "comboBoxWorkGruopsChat";
            this.comboBoxWorkGruopsChat.Size = new System.Drawing.Size(186, 21);
            this.comboBoxWorkGruopsChat.TabIndex = 1;
            this.comboBoxWorkGruopsChat.SelectedIndexChanged += new System.EventHandler(this.comboBoxWorkGruopsChat_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Ваше сообщение";
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(108, 318);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(340, 20);
            this.textBoxMessage.TabIndex = 3;
            // 
            // buttonSend
            // 
            this.buttonSend.Location = new System.Drawing.Point(455, 316);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(104, 23);
            this.buttonSend.TabIndex = 4;
            this.buttonSend.Text = "Отправить";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // buttonUpdateChat
            // 
            this.buttonUpdateChat.Location = new System.Drawing.Point(199, 5);
            this.buttonUpdateChat.Name = "buttonUpdateChat";
            this.buttonUpdateChat.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateChat.TabIndex = 5;
            this.buttonUpdateChat.Text = "Обновить";
            this.buttonUpdateChat.UseVisualStyleBackColor = true;
            this.buttonUpdateChat.Click += new System.EventHandler(this.comboBoxWorkGruopsChat_SelectedIndexChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.comboBoxSkills);
            this.tabPage5.Controls.Add(this.buttonApply);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.dateTimePickerBirthday);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.textBoxWorkTime);
            this.tabPage5.Controls.Add(this.textBoxSocial);
            this.tabPage5.Controls.Add(this.textBoxPassword);
            this.tabPage5.Controls.Add(this.textBoxEmail);
            this.tabPage5.Controls.Add(this.textBoxPhone);
            this.tabPage5.Controls.Add(this.textBoxName);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(565, 345);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Профиль";
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(235, 43);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(128, 20);
            this.textBoxName.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(177, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "ФИО";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(235, 68);
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(128, 20);
            this.textBoxPhone.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(177, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Телефон";
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Location = new System.Drawing.Point(235, 92);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(128, 20);
            this.textBoxEmail.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(177, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Email";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(235, 118);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(128, 20);
            this.textBoxPassword.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(177, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Пароль";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxSocial
            // 
            this.textBoxSocial.Location = new System.Drawing.Point(235, 146);
            this.textBoxSocial.Name = "textBoxSocial";
            this.textBoxSocial.Size = new System.Drawing.Size(128, 20);
            this.textBoxSocial.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(177, 147);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Соцсети";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxWorkTime
            // 
            this.textBoxWorkTime.Location = new System.Drawing.Point(235, 170);
            this.textBoxWorkTime.Name = "textBoxWorkTime";
            this.textBoxWorkTime.Size = new System.Drawing.Size(128, 20);
            this.textBoxWorkTime.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(177, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "График";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTimePickerBirthday
            // 
            this.dateTimePickerBirthday.Location = new System.Drawing.Point(180, 249);
            this.dateTimePickerBirthday.Name = "dateTimePickerBirthday";
            this.dateTimePickerBirthday.Size = new System.Drawing.Size(183, 20);
            this.dateTimePickerBirthday.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(177, 193);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Специальность";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(200, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(147, 33);
            this.label16.TabIndex = 4;
            this.label16.Text = "Профиль";
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(180, 275);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(187, 23);
            this.buttonApply.TabIndex = 5;
            this.buttonApply.Text = "Применить";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonNapominanie
            // 
            this.buttonNapominanie.Location = new System.Drawing.Point(247, 146);
            this.buttonNapominanie.Name = "buttonNapominanie";
            this.buttonNapominanie.Size = new System.Drawing.Size(187, 23);
            this.buttonNapominanie.TabIndex = 7;
            this.buttonNapominanie.Text = "Поставить напоминание";
            this.buttonNapominanie.UseVisualStyleBackColor = true;
            // 
            // comboBoxSkills
            // 
            this.comboBoxSkills.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSkills.FormattingEnabled = true;
            this.comboBoxSkills.Location = new System.Drawing.Point(180, 209);
            this.comboBoxSkills.Name = "comboBoxSkills";
            this.comboBoxSkills.Size = new System.Drawing.Size(183, 21);
            this.comboBoxSkills.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(177, 233);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Дата рождения";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 384);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "FormAdmin";
            this.Text = "FormAdmin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAdmin_FormClosing);
            this.Load += new System.EventHandler(this.FormAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkers)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkGroups)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGoals)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewWorkers;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridViewWorkGroups;
        private System.Windows.Forms.Button buttonDownloadAllWorkGroups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxWorkGroupName;
        private System.Windows.Forms.RichTextBox richTextBoxComments;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonDeleteWorkGroup;
        private System.Windows.Forms.Button buttonUpdateWorkGroup;
        private System.Windows.Forms.Button buttonCreateWorkGroup;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBoxDesc;
        private System.Windows.Forms.Button buttonUpdateGoal;
        private System.Windows.Forms.Button buttonDeleteGoal;
        private System.Windows.Forms.Button buttonAddGoal;
        private System.Windows.Forms.TextBox textBoxGoalName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxGoalStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewGoals;
        private System.Windows.Forms.Button buttonAddWorker;
        private System.Windows.Forms.Button buttonDeleteWorker;
        private System.Windows.Forms.ComboBox comboBoxWorkers;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxWorkGroups;
        private System.Windows.Forms.Button buttonUpdateWorkers;
        private System.Windows.Forms.ComboBox comboBoxDeleteWorker;
        private System.Windows.Forms.Button buttonDownloadAllGoals;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxWorkGruopsChat;
        private System.Windows.Forms.RichTextBox richTextBoxChat;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Button buttonUpdateChat;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirthday;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxWorkTime;
        private System.Windows.Forms.TextBox textBoxSocial;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button buttonNapominanie;
        private System.Windows.Forms.ComboBox comboBoxSkills;
        private System.Windows.Forms.Label label15;
    }
}